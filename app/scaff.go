package main

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"os"
	"os/user"
	"log"
	"io"
	"flag"
	"net/http"
	"strings"
)

const scaffoldBaseURL="https://scaffold.cloudy76.com/templates"

type Response struct {
    Count	string		`json:"Count"`
    Items	[]Items		`json:"Items"`
}

type Items struct {
    Name	string		`json:"name"`
    Tags	[]string	`json:"tags"`
    Description	string		`json:"description"`
}


func main() {

	if (len(os.Args) < 2) {
		showUsage()
		os.Exit(1)
	}

	createCommand := flag.NewFlagSet("create", flag.ExitOnError)

	projName := createCommand.String("project", "default", "Name for the new project")
	templateName := createCommand.String("template", "default", "Name for template to use")

	searchCommand := flag.NewFlagSet("search", flag.ExitOnError)
	searchQuery:= searchCommand.String("query", "default", "Name for template to use")

	switch os.Args[1] {
	case "create":
		createCommand.Parse(os.Args[2:])
		createScaffold(*projName, *templateName)
	case "search":
		searchCommand.Parse(os.Args[2:])
		searchForTemplate(*searchQuery)
	default:
		showUsage()
		os.Exit(1)
	}

}

func searchForTemplate(q string) {

	response, err := http.Get("https://scaffold.cloudy76.com/api/search?q="+q)
	if err != nil {
		fmt.Printf("The HTTP request failed with error %s\n", err)
	} else {
		body, _ := ioutil.ReadAll(response.Body)

		var resObj Response
		json.Unmarshal(body, &resObj)
		for i := 0; i < len(resObj.Items); i++ {
			fmt.Printf("%-18s %s %s\n", resObj.Items[i].Name, resObj.Items[i].Description, resObj.Items[i].Tags)
		}

	}
}

func createScaffold(projName, templateName string) {

	create_config_dir()
	log.Print("Creating project "+projName)

	if !templateExists(templateName) {
		log.Print("Not found locally - downloading")
		log.Print(scaffoldBaseURL+"/"+templateName+".zip")
		if err := DownloadFile(templateName+".zip", scaffoldBaseURL+"/"+templateName+".zip"); err != nil {
			panic(err)
		}
		files, err := Unzip(templateName+".zip", getTemplatePath(""))
		if err != nil {
			log.Fatal(err)
		}

		fmt.Println("Unzipped:\n" + strings.Join(files,"\n"))

	} else {
		log.Print("Already exists locally")
	}

	if _, err := os.Stat(projName); os.IsNotExist(err) {
		createFromTemplate(projName, templateName)
        } else {
		log.Fatal("Project already exists with that name");
	}

}

func createFromTemplate(projName, templateName string) {
	os.MkdirAll(projName, os.ModePerm)
	log.Print(getTemplatePath(templateName), ".")
	CopyDirectory(getTemplatePath(templateName), projName)
}


func templateExists(templateName string) bool {

	templatePath := getTemplatePath(templateName)
	if _, err := os.Stat(templatePath); os.IsNotExist(err) {
		return false
	} else {
		return true
	}
}

func getTemplatePath(templateName string) string {
	usr, err := user.Current()
	if err != nil {
		log.Fatal( err )
	}

	return usr.HomeDir + "/.scaff/templates/" + templateName
}

func create_config_dir() {

	usr, err := user.Current()
	if err != nil {
		log.Fatal( err )
	}

	homedir := usr.HomeDir + "/.scaff"

	if _, err := os.Stat(homedir); os.IsNotExist(err) {
		os.MkdirAll(homedir + "/templates", os.ModePerm)
		log.Print("Made config dir at "+homedir)
	}

}

func DownloadFile(filepath string, url string) error {

    // Get the data
    resp, err := http.Get(url)
    if err != nil {
        return err
    }
    defer resp.Body.Close()

    // Create the file
    out, err := os.Create(filepath)
    if err != nil {
        return err
    }
    defer out.Close()

    // Write the body to file
    _, err = io.Copy(out, resp.Body)
    return err
}

func showUsage() {
	fmt.Println("Usage: ", os.Args[0], "<command> <options>")
	fmt.Println()
	fmt.Println("Commands:")
	fmt.Println("  create")
	fmt.Println("    --project <project name>		The name of the project you want to create")
	fmt.Println("    --template <template name>		The template to use")
	fmt.Println("  search")
	fmt.Println("    --query <string>			Search for a template with specified keywords")
	fmt.Println()
}
