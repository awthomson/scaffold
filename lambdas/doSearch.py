import json
import boto3
import decimal
from boto3.dynamodb.conditions import Key, Attr

# Helper class to convert a DynamoDB item to JSON.
class DecimalEncoder(json.JSONEncoder):
    def default(self, o):
        if isinstance(o, decimal.Decimal):
            if o % 1 > 0:
                return float(o)
            else:
                return int(o)
        return super(DecimalEncoder, self).default(o)

def lambda_handler(event, context):
    
    q = event['queryStringParameters']['q']
    
    dynamodb = boto3.resource('dynamodb')
    table = dynamodb.Table('scaffold-templates')
    
    fe = Attr('tags').contains(q.upper())
    response = table.scan(
        FilterExpression=fe
    )

    f = '{"Count": '+str(response["Count"])+', "Items": '+json.dumps(response["Items"], cls=DecimalEncoder)+'}'
    
    return {
        'statusCode': 200, 
        'body': f
    }

