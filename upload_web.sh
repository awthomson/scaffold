find web/ -name "*.js" -exec aws s3 cp {} s3://scaffold.cloudy76.com/{} --metadata 'Content-Type=text/javascript' --acl 'public-read' \;
find web/ -name "*.html" -exec aws s3 cp {} s3://scaffold.cloudy76.com/{} --metadata 'Content-Type=text/html' --acl 'public-read' \;
find web/ -name "*.css" -exec aws s3 cp {} s3://scaffold.cloudy76.com/{} --metadata 'Content-Type=text/stylecheet' --acl 'public-read' \;
find web/ -name "*.jpg" -o -name "*.png" -o -name "*.svg" -exec aws s3 cp {} s3://scaffold.cloudy76.com/{} --metadata 'Content-Type=binary/image' --acl 'public-read' \;
find web/ -name "*.jpg" -exec aws s3 cp {} s3://scaffold.cloudy76.com/{} --metadata 'Content-Type=binary/image' --acl 'public-read' \;

