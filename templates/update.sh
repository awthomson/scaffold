#!/bin/bash

zip -r c.zip c
zip -r cpp.zip cpp
zip -r go.zip go
zip -r springboot.zip springboot
zip -r springboot-rest-api.zip springboot-rest-api
aws s3 cp c.zip s3://scaffold.cloudy76.com/templates/ --acl public-read
aws s3 cp cpp.zip s3://scaffold.cloudy76.com/templates/ --acl public-read
aws s3 cp springboot.zip s3://scaffold.cloudy76.com/templates/ --acl public-read
aws s3 cp springboot-rest-api.zip s3://scaffold.cloudy76.com/templates/ --acl public-read
rm *.zip
