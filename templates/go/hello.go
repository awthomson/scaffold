package main

import (
	"flag"
	"fmt"
	"log"
	"os"
)

func main() {

  showHelp:= flag.Bool("help", false, "Show usage information")
  name:= flag.String("name", "world", "Name of person")
  flag.Parse()

  if (*showHelp) {
		flag.Usage()
		os.Exit(1)
	}
  
  fmt.Println("Hello, ", *name)
  log.Print("Hello, ", *name)

}

