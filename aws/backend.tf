provider "aws" {
  version = "~> 2.0"
  region  = "ap-southeast-2"
}

/*
  Roles & Policies
*/
resource "aws_iam_role" "iam_for_lambda" {
  name = "cloudy76-scaffold-lambda-role"

  assume_role_policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": "sts:AssumeRole",
      "Principal": {
        "Service": "lambda.amazonaws.com"
      },
      "Effect": "Allow",
      "Sid": ""
    }
  ]
}
EOF
}

resource "aws_iam_role_policy" "iam_for_lambda_policy" {
  name = "cloudy76-scaffold-lambda-policy"
  role = aws_iam_role.iam_for_lambda.id

  policy = <<-EOF
  {
    "Version": "2012-10-17",
    "Statement": [
        {
            "Action": [
                "cloudwatch:DescribeAlarmHistory",
                "cloudwatch:DescribeAlarms",
                "cloudwatch:DescribeAlarmsForMetric",
                "cloudwatch:GetMetricStatistics",
                "cloudwatch:ListMetrics",
                "dynamodb:BatchGetItem",
                "dynamodb:Describe*",
                "dynamodb:List*",
                "dynamodb:GetItem",
                "dynamodb:Query",
                "dynamodb:Scan",
                "iam:GetRole",
                "iam:ListRoles"
            ],
            "Effect": "Allow",
            "Resource": "*"
        }
    ]
  }
  EOF
}

/*
  Lambdas
*/
resource "aws_lambda_function" "lambda_download" {
  filename      = "download.zip"
  function_name = "cloudy76-scaffold-download"
  role          = aws_iam_role.iam_for_lambda.arn
  handler       = "download.lambda_handler"

  source_code_hash = filebase64sha256("download.zip")

  runtime = "python3.7"

}

resource "aws_lambda_function" "lambda_search" {
  filename      = "search.zip"
  function_name = "cloudy76-scaffold-search"
  role          = aws_iam_role.iam_for_lambda.arn
  handler       = "search.lambda_handler"

  source_code_hash = filebase64sha256("search.zip")

  runtime = "python3.7"

}
