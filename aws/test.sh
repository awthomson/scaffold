#!/bin/bash

# Test
PAYLOAD=$(echo -n '{ "queryStringParameters": { "q": "C" } }' | base64 )
aws lambda invoke --function-name cloudy76-scaffold-search --payload ${PAYLOAD} response.json
cat response.json
echo ""
echo ""
rm response.json

