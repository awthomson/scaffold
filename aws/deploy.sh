#!/bin/bash

prep () {
  echo "Prep..."
  if [ -e search.zip ]; then
    rm search.zip
  fi
  if [ -e download.zip ]; then
    rm download.zip
  fi

  zip search.zip search.py
  zip download.zip download.py 
}

terraformer () {
  echo "Terraforming..."
  terraform apply
}

gateway () {
  echo "Create API..."
  aws apigatewayv2 create-api --name "cloudy76-scaffold-APIs" --protocol-type HTTP --route-selection-expression '${request.method} ${request.path}' > a.json
  API_ID=$(jq -r .ApiId a.json)

  echo "Create Stage..."
  aws apigatewayv2 create-stage --stage-name "prod"  --api-id ${API_ID} --auto-deploy > a.json

  echo "Create Search Integration..."
  aws apigatewayv2 create-integration --api-id ${API_ID} --integration-type AWS_PROXY --integration-uri "arn:aws:lambda:ap-southeast-2:170004535943:function:cloudy76-scaffold-search" --payload-format-version "1.0" > a.json
  INTEGRATION_ID=$(jq -r .IntegrationId a.json)

  echo "Create Search Route..."
  aws apigatewayv2 create-route --api-id ${API_ID} --route-key 'GET /api/search' --target "integrations/${INTEGRATION_ID}" > a.json

  echo "Create Download Integration..."
  aws apigatewayv2 create-integration --api-id ${API_ID} --integration-type AWS_PROXY --integration-uri "arn:aws:lambda:ap-southeast-2:170004535943:function:cloudy76-scaffold-download" --payload-format-version "1.0" > a.json
  INTEGRATION_ID=$(jq -r .IntegrationId a.json)

  echo "Create Download Route..."
  aws apigatewayv2 create-route --api-id ${API_ID} --route-key 'GET /api/download/{name}' --target "integrations/${INTEGRATION_ID}" > a.json
}

tidy () {
  echo "Tidy..."
  rm *.zip a.json
}

# Test
testLambdas () {
  echo "Testing..."
  echo "   * search"
  PAYLOAD=$(echo -n '{ "queryStringParameters": { "q": "C" } }' | base64 )
  aws lambda invoke --function-name cloudy76-scaffold-search --payload ${PAYLOAD} response.json
  cat response.json
  echo ""
  echo ""
  echo "   * download"
  PAYLOAD=$(echo -n '{ "pathParameters": { "name": "C" } }' | base64 )
  aws lambda invoke --function-name cloudy76-scaffold-download --payload ${PAYLOAD} response.json
  cat response.json
  echo ""
  echo ""
  rm response.json
}

prep
terraformer
gateway
tidy
testLambdas

