import boto3
import json

def lambda_handler(event, context):
    
    q = event['pathParameters']['name']
    
    return {
        'statusCode': 200, 
        'body': q
    }

